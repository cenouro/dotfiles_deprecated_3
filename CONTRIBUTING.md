## Repository

Revise commits and changes before merging. If possible, wait a little while
before merging. Resting before merging usually allows further insights and
reduce typos on commit message.

## Commit Message Guidelines

References:

- [Angular](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)
- [Linux Kernel](https://github.com/torvalds/linux/commits/master)

Commit should follow the following structure:

```
[<type>.]<module>: <title> #<issue_number>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
<BLANK LINE>
```

#### Module

Use what best describes the "module":

- **vim** for Vim/vimrc and plugins related stuff
- **qa** for things that act as quality assurance for this repository, such as
  CI (.gitlab-ci.yml) and git hooks
- **doc** for generic documentation (CONTRIBUTING.md)
- **setup** for rakefiles and Ansible playbooks
- **git** for generic Git stuff

#### Type

Can be:

- **fix** for bug fixes
- **doc** when only module's documentation is added/modified

#### Title

Follow normal git convention. Imperative, first letter capitalized. **No
trailing period (.).**

#### Body

Describe what caused the bug, how it was fixed, why code was refactored, why a
plugin was removed and so on. Give reasons, please.

