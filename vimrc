set nocompatible
" vim: foldmethod=marker : spell : spelllang=en_us
filetype plugin indent on
syntax on
let mapleader = ","

call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'leafgarland/typescript-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'pangloss/vim-javascript'
Plug 'rakr/vim-one'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-sensible'
call plug#end()

" Basic configs {{{
autocmd BufWritePre * :%s/\s\+$//e
set backspace=start,eol,indent
set cursorline
set diffopt+=algorithm:patience,indent-heuristic
set encoding=utf-8
set hidden
set modeline
set noswapfile nobackup
set number relativenumber
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab autoindent smartindent
set termguicolors
" }}}
" Augroups {{{
augroup colorscheme_tweaks
    autocmd!
    autocmd ColorScheme one
                \ highlight SpellBad cterm=underline |
                \ highlight SpellCap cterm=underline |
                \ highlight SpellLocal cterm=underline |
                \ highlight SpellRare cterm=underline
augroup END

augroup ft_spell
    autocmd!
    autocmd FileType gitcommit,markdown setlocal spell spelllang=en_us
augroup END
" }}}

let g:lightline = { 'colorscheme': 'one' }
colorscheme one
set background=dark

" Coc configuration {{{
source $HOME/.vim/coc.vim " Sample configuration file from github

" Scroll completion
inoremap <expr> <c-j> pumvisible() ? "\<C-n>" : coc#refresh()
inoremap <expr> <c-k> pumvisible() ? "\<C-p>" : coc#refresh()

" Scroll documentation
if has('nvim-0.4.0') || has('patch-8.2.0750')
    nnoremap <silent><nowait><expr> <C-u> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-u>"
    nnoremap <silent><nowait><expr> <C-i> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-i>"
    inoremap <silent><nowait><expr> <C-u> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<c-u>"
    inoremap <silent><nowait><expr> <C-i> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<c-i>"
    vnoremap <silent><nowait><expr> <C-u> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-u>"
    vnoremap <silent><nowait><expr> <C-i> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-i>"
endif
" }}}
" vim-projectionist configuration {{{
let g:projectionist_heuristics = {
            \ "pom.xml": {
            \     "*": {"make": "mvn"},
            \     "src/main/java/*.java": {
            \         "type": "source",
            \         "alternate": "src/test/java/{}Test.java"
            \     },
            \     "src/test/java/*Test.java": {
            \         "type": "test",
            \         "alternate": "src/main/java/{}.java"
            \     }
            \ },
            \ "node_modules/": {
            \     "*": {"make": "npm test"},
            \     "src/*.ts": {
            \         "type": "source",
            \         "alternate": "test/{}.spec.ts"
            \     },
            \     "test/*.spec.ts": {
            \         "type": "test",
            \         "alternate": "src/{}.ts"
            \     }
            \ }}
" }}}
" Mappings {{{
cnoremap Git vert Git
nnoremap <leader>t :make test<cr>
nnoremap <s-tab> :bprevious<cr>
nnoremap <tab> :bnext<cr>
nnoremap ga :A<cr>
" }}}

